#!/usr/bin/env ruby

def ReadReference(path)
  bibFile = open path
  reference = bibFile.read
  bibFile.close
  return reference 
end

def LoadReferences(path)

  references = Array.new
  
  Dir.each_child(path) do |file|
    if File.directory? file
      references = references | (LoadReferences file)
    elsif (file.end_with? ".bib") and file != "references.bib"
      references.append(ReadReference file)
    end
  end
  return references 
end

referenceFile = "references.bib"
if File.exists? referenceFile
  File.delete(referenceFile)
end

references = LoadReferences(".")


referenceFile = open(referenceFile, "a")
referenceFile.puts references
referenceFile.close


