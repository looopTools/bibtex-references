# Bibtex References

This repository contains my collection of BibTex references for acadmeic papers, tools, and books. 

The plan is to organise the references based on topic into separate directories (and sub-directories).
To create a combined reference file there is a script called `combine.rb` to run the combination script, simply run `ruby combine.rb`. 
This will combine any file ending in `.bib` in the project root directories and sub-directories  and compile a file named `references.bib` which can be used in any LaTeX project and works with Emacs AucTex and Org modes Org-Ref. 

If you have `references.bib` file in the root folder already or in a sub-directory, it will not be include in the compiled references file and the root folders references.bib will be deleted and a new one will be recreated.   

